import {combineReducers} from "redux";
import { loadingBarReducer } from 'react-redux-loading-bar';
import articleListReducer from '../app/containers/ArticleListContainer/reducer';
import categoryListReducer from '../app/containers/CategoryListContainer/reducer';
import articleReducer from "../app/containers/ArticleContainer/reducer";
import categoryReducer from "../app/containers/CategoryContainer/reducer";
import socialListReducer from "../app/containers/SocialListContainer/reducer";
import contactReducer from "../app/containers/ContactContainer/reducer";

const rootReducer = combineReducers({
    articles: articleListReducer,
    topics: categoryListReducer,
    article: articleReducer,
    topic: categoryReducer,
    socials: socialListReducer,
    contact: contactReducer,
    loadingBar: loadingBarReducer,
});

export default rootReducer;