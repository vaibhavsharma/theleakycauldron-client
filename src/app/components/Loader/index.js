/**
 * Created by vaibhav on 20-10-2017.
 */
import React from 'react';
import threedots from '../../../resources/images/threedots.svg';
import './style.css';

const Loader = () => {
    return (
        <div className="loader text-center">
            <img src={threedots} alt="loader"/>
        </div>
    );
};

export default Loader;