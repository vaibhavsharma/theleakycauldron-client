/**
 * Created by vaibhav on 6/11/17
 */
import React from 'react';
import {PropTypes} from 'prop-types';

const TextField = ({size, name, label, onChange, placeholder, value, error}) => {
    return (
        <div className={size}>
            <div className="form-group">
                <label className="control-label">{label}</label>
                <textarea className="form-control" name={name} placeholder={placeholder} value={value} onChange={onChange} rows="5"/>
                {error && <p><strong>{error}</strong></p>}
            </div>
        </div>
    );
};

TextField.propTypes = {
    size: PropTypes.string,
    name: PropTypes.string.isRequired,
    label: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    error: PropTypes.string
};

export default TextField;