/**
 * Created by vaibhav on 20-10-2017.
 */
import React from 'react';
import {PropTypes} from 'prop-types';
import {Link} from 'react-router-dom';
import Markdown from 'react-remarkable';

const ArticleRow = ({article_prop}) => {
    const d = new Date(article_prop.publish_date);
    return (
        <article>
            <header>
                <h1><Link to={"/article/" + article_prop.slug}>{article_prop.title}</Link></h1>
                <p><span className="label label-default">{article_prop.category}</span></p>
                <p>{d.toDateString()} | @{article_prop.author}</p>
            </header>
            <Link to={"/article/" + article_prop.slug} className="img-responsive thumbnail">
                <img src={article_prop.image} alt={article_prop.title}/>
            </Link>
            <Markdown source={article_prop.content.substring(0, 350)}/>
            <Link to={"/article/" + article_prop.slug} className="btn btn-primary">
                Continue Reading
            </Link>
            <hr/>
        </article>
    );
};

ArticleRow.propTypes = {
    article_prop: PropTypes.object.isRequired
};

export default ArticleRow;