/**
 * Created by vaibhav on 20-10-2017.
 */
import React from 'react';
import {Link, NavLink} from 'react-router-dom';
import LoadingBar from 'react-redux-loading-bar';
import CategoryListContainer from '../../containers/CategoryListContainer';

const Header = () => {
    return (
        <nav className="navbar navbar-default navbar-fixed-top">
            <LoadingBar style={{ backgroundColor: '#ed786a', zIndex: 999 }}/>
            <div className="container">
                <div className="navbar-header">
                    <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar"/>
                        <span className="icon-bar"/>
                        <span className="icon-bar"/>
                    </button>
                    <Link className="navbar-brand" to="/">The Leaky Cauldron Blog</Link>
                </div>

                <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul className="nav navbar-nav navbar-right">
                        <li><NavLink to="/about" activeClassName="active">About</NavLink></li>
                        <li className="dropdown">
                            <a type="button" className="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false">
                                Category <span className="caret"/>
                            </a>
                            <CategoryListContainer/>
                        </li>
                        <li><NavLink to="/contact" activeClassName="active">Contact</NavLink></li>
                    </ul>
                </div>
            </div>
        </nav>
    );
};

export default Header;