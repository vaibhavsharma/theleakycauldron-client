/**
 * Created by vaibhav on 20-10-2017.
 */
import React from 'react';
import {Route, Switch} from 'react-router-dom';
import './style.css';
import asyncComponent from "../AsyncComponent";

const AsyncHomePage = asyncComponent(() => import("../../pages/HomePage"));
const AsyncAboutPage = asyncComponent(() => import("../../pages/AboutPage"));
const AsyncArticlePage = asyncComponent(() => import("../../pages/ArticlePage"));
const AsyncCategoryPage = asyncComponent(() => import("../../pages/CategoryPage"));
const AsyncContactPage = asyncComponent(() => import("../../pages/ContactPage"));
const AsyncNotFoundPage = asyncComponent(() => import("../../pages/NotFoundPage"));

const Main = () => {
    return (
        <div id="wrapper">
            <Switch>
                <Route exact path="/" component={AsyncHomePage}/>
                <Route path="/about" component={AsyncAboutPage}/>
                <Route path="/category/:slug" component={AsyncCategoryPage}/>
                <Route path="/article/:slug" component={AsyncArticlePage}/>
                <Route path="/contact" component={AsyncContactPage}/>
                <Route exact path="/index.html" component={AsyncHomePage}/>
                <Route component={AsyncNotFoundPage}/>
            </Switch>
        </div>
    );
};

export default Main;