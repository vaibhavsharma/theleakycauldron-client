/**
 * Created by vaibhav on 20-10-2017.
 */
import React from 'react';
import {Link} from 'react-router-dom';
import './style.css';
import SocialListContainer from "../../containers/SocialListContainer";

const Footer = () => {
    return (
        <footer className="footer">
            <div className="container">
                <div className="row">
                    <div className="col-lg-6 pull-left">
                        <SocialListContainer/>
                        <p>
                            Made by <a href="mailto:vaibhav@theleakycauldronblog.com">Vaibhav Sharma</a>
                        </p>
                        <p>Copyright &copy; 2017 <Link to="/">The Leaky Cauldron</Link>. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export default Footer;