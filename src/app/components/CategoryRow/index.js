/**
 * Created by vaibhav on 20-10-2017.
 */
import React from 'react';
import {PropTypes} from 'prop-types';
import {Link} from 'react-router-dom';

const CategoryRow = ({category_prop}) => {
    return (
        <li><Link to={"/category/" + category_prop.slug}>{category_prop.category_name}</Link></li>
    );
};

CategoryRow.propTypes = {
    category_prop: PropTypes.object.isRequired
};

export default CategoryRow;