/**
 * Created by vaibhav on 6/11/17
 */
import React from 'react';
import {PropTypes} from 'prop-types';

const SocialRow = ({social_prop}) => {
    return (
        <li><a href={social_prop.url} rel="noopener noreferrer" target="_blank">{social_prop.title}</a></li>
    );
};

SocialRow.propTypes = {
    social_prop: PropTypes.object.isRequired
};

export default SocialRow;