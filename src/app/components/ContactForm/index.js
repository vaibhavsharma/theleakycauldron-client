/**
 * Created by vaibhav on 6/11/17
 */
import React from 'react';
import {PropTypes} from 'prop-types';

import CharField from "../CharField";
import TextField from "../TextField";

const ContactForm = ({name, email, message, errors, loading, onChange, onSave}) => {
    return (
        <form>
            <div className="row">
                <CharField
                    size="col-sm-6"
                    label='First Name*'
                    name="first_name"
                    value={name}
                    placeholder="Jane"
                    onChange={onChange}
                    error={errors.title}/>

                <CharField
                    size="col-sm-6"
                    label='Last Name*'
                    name="last_name"
                    value={name}
                    placeholder="Doe"
                    onChange={onChange}
                    error={errors.title}/>
            </div>
            <div className="row">
                <CharField
                    size="col-sm-12"
                    label='Email*'
                    name="email"
                    value={email}
                    placeholder="Email eg: someone@examle.com"
                    onChange={onChange}
                    error={errors.title}/>
            </div>
            <div className="row">
                <TextField
                    size="col-sm-12"
                    name="message"
                    label="Message*"
                    value={message}
                    placeholder="Write Your Message Here..."
                    onChange={onChange}
                    error={errors.title}/>
            </div>
            <div className="row">
                <div className="col-sm-12">
                    <input
                        type="submit"
                        disabled={loading}
                        className="btn btn-primary btn-block"
                        onClick={onSave}
                        value={loading ? '• • •' : 'Send Message'}/>
                </div>
            </div>
        </form>
    );
};

ContactForm.propTypes = {
    name: PropTypes.string,
    email: PropTypes.string,
    message: PropTypes.string,
    loading: PropTypes.bool,
    onSave: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    errors: PropTypes.object
};

export default ContactForm;