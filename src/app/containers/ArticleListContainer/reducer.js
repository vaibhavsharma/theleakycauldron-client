import {FETCH_ARTICLE_FAILURE, FETCH_ARTICLE_REQUEST, FETCH_ARTICLE_SUCCESS} from "./action";

const INITIAL_STATE = {articleList: {articles: [], error: null, loading: false}};

export default function articleListReducer(state = INITIAL_STATE, action) {
    let error;
    switch (action.type) {
        case FETCH_ARTICLE_REQUEST:
            return {...state, articleList: {articles: [], error: null, loading: true}};
        case FETCH_ARTICLE_SUCCESS:
            return {...state, articleList: {articles: action.payload, error: null, loading: false}};
        case FETCH_ARTICLE_FAILURE:
            error = action.payload || {message: action.payload.message};
            return {...state, articleList: {articles: [], error: error, loading: false}};
        default:
            return state;
    }
}