/**
 * Created by vaibhav on 20-10-2017.
 */
import React, {Component} from 'react';
import {PropTypes} from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from "./action";
import ArticleRow from '../../components/ArticleRow';


class ArticleList extends Component {
    componentDidMount() {
        this.props.actions.fetchArticleList();
    }

    render() {
        const {articleList} = this.props;
        return (
            <div className="container">
                {articleList.loading ?
                    null
                    :
                    articleList.articles.map(article =>
                        <ArticleRow key={article.id} article_prop={article}/>
                    )}
            </div>
        );
    }
}

ArticleList.propTypes = {
    articleList: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
    return {
        articleList: state.articles.articleList
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    };
}

const ArticleListContainer = connect(mapStateToProps, mapDispatchToProps)(ArticleList);

export default ArticleListContainer;