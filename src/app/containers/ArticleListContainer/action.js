import axios from 'axios';
import {showLoading, hideLoading} from 'react-redux-loading-bar';

export const FETCH_ARTICLE_REQUEST = 'FETCH_ARTICLE_REQUEST';
export const FETCH_ARTICLE_SUCCESS = 'FETCH_ARTICLE_SUCCESS';
export const FETCH_ARTICLE_FAILURE = 'FETCH_ARTICLE_FAILURE';
const ROOT_URL = 'https://tlcb.pythonanywhere.com/api';

export function fetchRequest() {
    return {
        type: FETCH_ARTICLE_REQUEST
    };
}

export function fetchSuccess(data) {
    return {
        type: FETCH_ARTICLE_SUCCESS,
        payload: data
    };
}

export function fetchFailure(error) {
    return {
        type: FETCH_ARTICLE_FAILURE,
        payload: error
    };
}

export function fetchArticleList() {
    return function (dispatch) {
        dispatch(showLoading());
        dispatch(fetchRequest());
        return axios({
            method: 'get',
            url: `${ROOT_URL}/article/?format=json`,
            headers: []
        }).then(function (response) {
            dispatch(hideLoading());
            !response.error ? dispatch(fetchSuccess(response.data)) : dispatch(fetchFailure(response.data));
        }).catch(function (error) {
            console.log(error);
        });
    };
}