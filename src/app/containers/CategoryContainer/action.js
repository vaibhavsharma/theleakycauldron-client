import axios from 'axios';
import {showLoading, hideLoading} from 'react-redux-loading-bar';

export const FETCH_CATEGORY_REQUEST = 'FETCH_CATEGORY_REQUEST';
export const FETCH_CATEGORY_SUCCESS = 'FETCH_CATEGORY_SUCCESS';
export const FETCH_CATEGORY_FAILURE = 'FETCH_CATEGORY_FAILURE';
const ROOT_URL = 'https://tlcb.pythonanywhere.com/api';

export function fetchRequest() {
    return {
        type: FETCH_CATEGORY_REQUEST
    };
}

export function fetchSuccess(data) {
    return {
        type: FETCH_CATEGORY_SUCCESS,
        payload: data
    };
}

export function fetchFailure(error) {
    return {
        type: FETCH_CATEGORY_FAILURE,
        payload: error
    };
}

export function fetchArticleList(slug) {
    return function (dispatch) {
        dispatch(showLoading());
        dispatch(fetchRequest());
        return axios({
            method: 'get',
            url: `${ROOT_URL}/article/category/${slug}/?format=json`,
            headers: []
        }).then(function (response) {
            dispatch(hideLoading());
            !response.error ? dispatch(fetchSuccess(response.data)) : dispatch(fetchFailure(response.data));
        }).catch(function (error) {
            console.log(error);
        });
    };
}