/**
 * Created by vaibhav on 20-10-2017.
 */
import React, {Component} from 'react';
import {PropTypes} from 'prop-types';
import {Helmet} from "react-helmet";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import * as actions from "./action";
import ArticleRow from '../../components/ArticleRow';


class Category extends Component {
    componentDidMount(urlParam = this.props.match.params.slug) {
        this.props.actions.fetchArticleList(urlParam);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.match.params.slug !== this.props.match.params.slug) {
            this.props.actions.fetchArticleList(nextProps.match.params.slug);
        }
    }

    render() {
        const {topicObject} = this.props;
        return (
            <div className="container">
                <Helmet>
                    <title>{topicObject.category.meta_title}</title>
                    <meta name="description" content={topicObject.category.meta_description}/>
                </Helmet>
                {topicObject.loading ?
                    null
                    :
                    topicObject.category.articles.map(article =>
                        <ArticleRow key={article.id} article_prop={article}/>
                    )}
            </div>
        );
    }
}

Category.propTypes = {
    topicObject: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
};

function mapStateToProps(state, ownProps) {
    return {
        topicObject: state.topic.topicObject
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    };
}

const CategoryContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(Category));

export default CategoryContainer;