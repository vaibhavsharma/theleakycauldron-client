import {FETCH_CATEGORY_FAILURE, FETCH_CATEGORY_REQUEST, FETCH_CATEGORY_SUCCESS} from "./action";

const INITIAL_STATE = {topicObject: {category: {articles: []}, error: null, loading: false}};

export default function categoryReducer(state = INITIAL_STATE, action) {
    let error;
    switch (action.type) {
        case FETCH_CATEGORY_REQUEST:
            return {...state, topicObject: {category: {articles: []}, error: null, loading: true}};
        case FETCH_CATEGORY_SUCCESS:
            return {...state, topicObject: {category: action.payload, error: null, loading: false}};
        case FETCH_CATEGORY_FAILURE:
            error = action.payload || {message: action.payload.message};
            return {...state, topicObject: {category: {articles: []}, error: error, loading: false}};
        default:
            return state;
    }
}