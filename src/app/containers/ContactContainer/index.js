/**
 * Created by vaibhav on 6/11/17
 */
import React, {Component} from 'react';
import {PropTypes} from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from './action';
import ContactForm from '../../components/ContactForm';

class Contact extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            submitted: false,
            contact: Object.assign({}, props.contact),
            errors: {}
        };

        this.updateContactState = this.updateContactState.bind(this);
        this.sendContact = this.sendContact.bind(this);
    }

    updateContactState(event) {
        const field = event.target.name;
        let contact = this.state.contact;
        contact[field] = event.target.value;
        return this.setState({contact: contact});
    }

    sendContact(event) {
        event.preventDefault();
        this.props.actions.sendMessage(this.state.contact);
        this.setState({submitted: true});
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2">
                        <h1>Questions or Comments? Get in Touch:</h1>
                        <hr/>
                        {this.state.submitted ?
                            <div className="alert alert-dismissible alert-success">
                                <button type="button" className="close" data-dismiss="alert">&times;</button>
                                Your Message Has Been Sent, We'll Get Back To You Soon.
                            </div>
                            : null}
                        <ContactForm onSave={this.sendContact} onChange={this.updateContactState}
                                     errors={this.state.errors}/>
                    </div>
                </div>
            </div>
        );
    }
}

Contact.propTypes = {
    contact: PropTypes.object,
    actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
    return {
        contact: state.contact
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    };
}

const ContactContainer = connect(mapStateToProps, mapDispatchToProps)(Contact);

export default ContactContainer;