/**
 * Created by vaibhav on 6/11/17
 */
import {SEND_MESSAGE_FAILURE, SEND_MESSAGE_SUCCESS} from "./action";

export default function contactReducer(state = {}, action) {
    let error;
    switch (action.type) {
        case SEND_MESSAGE_SUCCESS:
            return [
                ...state,
                Object.assign({}, action.payload)
            ];
        case SEND_MESSAGE_FAILURE:
            error = action.payload || {message: action.payload.message};
            return [
                ...state,
                Object.assign({}, error)
            ];
        default:
            return state;
    }
}