/**
 * Created by vaibhav on 6/11/17
 */
import axios from 'axios';
import {showLoading, hideLoading} from 'react-redux-loading-bar';

export const SEND_MESSAGE_SUCCESS = 'SEND_MESSAGE_SUCCESS';
export const SEND_MESSAGE_FAILURE = 'SEND_MESSAGE_FAILURE';
const ROOT_URL = 'https://tlcb.pythonanywhere.com/api';

export function postSuccess(data) {
    return {
        type: SEND_MESSAGE_SUCCESS,
        payload: data
    };
}

export function postFailure(error) {
    return {
        type: SEND_MESSAGE_FAILURE,
        payload: error
    };
}

export function sendMessage(contact) {
    return function (dispatch) {
        dispatch(showLoading());
        return axios({
            method: 'post',
            url: `${ROOT_URL}/contact/`,
            headers: [],
            data: contact
        }).then(function (response) {
            dispatch(hideLoading());
            !response.error ? dispatch(postSuccess(response.data)) : dispatch(postFailure(response.data));
        }).catch(function (error) {
            console.log(error);
        });
    };
}