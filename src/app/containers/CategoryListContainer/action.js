import axios from 'axios';

export const FETCH_TOPICS_REQUEST = 'FETCH_TOPICS_REQUEST';
export const FETCH_TOPICS_SUCCESS = 'FETCH_TOPICS_SUCCESS';
export const FETCH_TOPICS_FAILURE = 'FETCH_TOPICS_FAILURE';
const ROOT_URL = 'https://tlcb.pythonanywhere.com/api';

export function fetchRequest() {
    return {
        type: FETCH_TOPICS_REQUEST
    };
}

export function fetchSuccess(data) {
    return {
        type: FETCH_TOPICS_SUCCESS,
        payload: data
    };
}

export function fetchFailure(error) {
    return {
        type: FETCH_TOPICS_FAILURE,
        payload: error
    };
}

export function fetchCategoryList() {
    return function (dispatch) {
        dispatch(fetchRequest());
        return axios({
            method: 'get',
            url: `${ROOT_URL}/category/?format=json`,
            headers: []
        }).then(function (response) {
            !response.error ? dispatch(fetchSuccess(response.data)) : dispatch(fetchFailure(response.data));
        }).catch(function (error) {
            console.log(error);
        });
    };
}
