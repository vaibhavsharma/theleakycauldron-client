import {FETCH_TOPICS_FAILURE, FETCH_TOPICS_REQUEST, FETCH_TOPICS_SUCCESS} from "./action";

const INITIAL_STATE = {categoryList: {topics: [], error: null, loading: false}};

export default function categoryListReducer(state = INITIAL_STATE, action) {
    let error;
    switch (action.type) {
        case FETCH_TOPICS_REQUEST:
            return {...state, categoryList: {topics: [], error: null, loading: true}};
        case FETCH_TOPICS_SUCCESS:
            return {...state, categoryList: {topics: action.payload, error: null, loading: false}};
        case FETCH_TOPICS_FAILURE:
            error = action.payload || {message: action.payload.message};
            return {...state, categoryList: {topics: [], error: error, loading: false}};
        default:
            return state;
    }
}