/**
 * Created by vaibhav on 20-10-2017.
 */
import React, {Component} from 'react';
import {PropTypes} from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as actions from "./action";
import Loader from "../../components/Loader";
import CategoryRow from '../../components/CategoryRow';

class CategoryList extends Component {
    componentWillMount() {
        this.props.actions.fetchCategoryList();
    }

    render() {
        const {categoryList} = this.props;
        return (
            <ul className="dropdown-menu" role="menu">
                {categoryList.loading ?
                    <Loader/>
                    :
                    categoryList.topics.map(topic =>
                        <CategoryRow key={topic.id} category_prop={topic}/>
                    )}
            </ul>
        );
    }
}

CategoryList.propTypes = {
    categoryList: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
    return {
        categoryList: state.topics.categoryList
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    };
}

const CategoryListContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(CategoryList));

export default CategoryListContainer;