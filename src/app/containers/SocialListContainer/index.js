/**
 * Created by vaibhav on 6/11/17
 */
import React, {Component} from 'react';
import {PropTypes} from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from "./action";
import SocialRow from '../../components/SocialRow';

class SocialList extends Component {
    componentWillMount() {
        this.props.actions.fetchSocialList();
    }

    render() {
        const {socialList} = this.props;
        return (
            <ul className="list-unstyled list-inline">
                {socialList.loading ?
                    <div className="container-fluid blink">• • •</div>
                    :
                    socialList.socials.map(social =>
                        <SocialRow key={social.id} social_prop={social}/>
                    )}
            </ul>
        );
    }
}

SocialList.propTypes = {
    socialList: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
    return {
        socialList: state.socials.socialList
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    };
}

const SocialListContainer = connect(mapStateToProps, mapDispatchToProps)(SocialList);

export default SocialListContainer;