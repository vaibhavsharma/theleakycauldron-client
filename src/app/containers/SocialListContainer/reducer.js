/**
 * Created by vaibhav on 6/11/17
 */
import {FETCH_SOCIAL_FAILURE, FETCH_SOCIAL_REQUEST, FETCH_SOCIAL_SUCCESS} from "./action";

const INITIAL_STATE = {socialList: {socials: [], error: null, loading: false}};

export default function socialListReducer(state = INITIAL_STATE, action) {
    let error;
    switch (action.type) {
        case FETCH_SOCIAL_REQUEST:
            return {...state, socialList: {socials: [], error: null, loading: true}};
        case FETCH_SOCIAL_SUCCESS:
            return {...state, socialList: {socials: action.payload, error: null, loading: false}};
        case FETCH_SOCIAL_FAILURE:
            error = action.payload || {message: action.payload.message};
            return {...state, socialList: {socials: [], error: error, loading: false}};
        default:
            return state;
    }
}