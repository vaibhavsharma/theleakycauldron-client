/**
 * Created by vaibhav on 6/11/17
 */
import axios from 'axios';

export const FETCH_SOCIAL_REQUEST = 'FETCH_SOCIAL_REQUEST';
export const FETCH_SOCIAL_SUCCESS = 'FETCH_SOCIAL_SUCCESS';
export const FETCH_SOCIAL_FAILURE = 'FETCH_SOCIAL_FAILURE';
const ROOT_URL = 'https://tlcb.pythonanywhere.com/api';

export function fetchRequest() {
    return {
        type: FETCH_SOCIAL_REQUEST
    };
}

export function fetchSuccess(data) {
    return {
        type: FETCH_SOCIAL_SUCCESS,
        payload: data
    };
}

export function fetchFailure(error) {
    return {
        type: FETCH_SOCIAL_FAILURE,
        payload: error
    };
}

export function fetchSocialList() {
    return function (dispatch) {
        dispatch(fetchRequest());
        return axios({
            method: 'get',
            url: `${ROOT_URL}/social/?format=json`,
            headers: []
        }).then(function (response) {
            !response.error ? dispatch(fetchSuccess(response.data)) : dispatch(fetchFailure(response.data));
        }).catch(function (error) {
            console.log(error);
        });
    };
}
