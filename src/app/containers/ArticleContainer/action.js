import axios from 'axios';
import {showLoading, hideLoading} from 'react-redux-loading-bar';

export const GET_ARTICLE_REQUEST = 'GET_ARTICLE_REQUEST';
export const GET_ARTICLE_SUCCESS = 'GET_ARTICLE_SUCCESS';
export const GET_ARTICLE_FAILURE = 'GET_ARTICLE_FAILURE';
const ROOT_URL = 'https://tlcb.pythonanywhere.com/api';

export function fetchRequest() {
    return {
        type: GET_ARTICLE_REQUEST
    };
}

export function fetchSuccess(data) {
    return {
        type: GET_ARTICLE_SUCCESS,
        payload: data
    };
}

export function fetchFailure(error) {
    return {
        type: GET_ARTICLE_FAILURE,
        payload: error
    };
}

export function getArticle(slug) {
    return function (dispatch) {
        dispatch(showLoading());
        dispatch(fetchRequest());
        return axios({
            method: 'get',
            url: `${ROOT_URL}/article/${slug}/?format=json`,
            headers: []
        }).then(function (response) {
            dispatch(hideLoading());
            !response.error ? dispatch(fetchSuccess(response.data)) : dispatch(fetchFailure(response.data));
        }).catch(function (error) {
            console.log(error);
        });
    };
}