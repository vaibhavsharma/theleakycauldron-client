/**
 * Created by vaibhav on 21-10-2017.
 */
import React, {Component} from 'react';
import {PropTypes} from 'prop-types';
import {Helmet} from "react-helmet";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Markdown from 'react-remarkable';
import * as actions from "./action";

class Article extends Component {
    componentDidMount() {
        this.props.actions.getArticle(this.props.match.params.slug);
    }

    render() {
        const {articleObject} = this.props;
        const d = new Date(articleObject.article.publish_date);
        return (
            <div className="container">
                <Helmet>
                    <title>{articleObject.article.meta_title}</title>
                    <meta name="description" content={articleObject.article.meta_description}/>
                </Helmet>
                {articleObject.loading ?
                    null
                    :
                    <article>
                        <header>
                            <h1>{articleObject.article.title}</h1>
                            <p><span className="label label-default">{articleObject.article.category}</span></p>
                            <p>{d.toDateString()} | {articleObject.article.author}</p>
                        </header>
                        <img src={articleObject.article.image} alt={articleObject.article.slug} className="img-responsive thumbnail"/>
                        <Markdown source={articleObject.article.content}/>
                    </article>
                }
            </div>
        );
    }
}

Article.propTypes = {
    articleObject: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
};

function mapStateToProps(state, ownProps) {
    return {
        articleObject: state.article.articleObject
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    };
}

const ArticleContainer = connect(mapStateToProps, mapDispatchToProps)(Article);

export default ArticleContainer;