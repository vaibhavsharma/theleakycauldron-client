import {GET_ARTICLE_FAILURE, GET_ARTICLE_REQUEST, GET_ARTICLE_SUCCESS} from "./action";

const INITIAL_STATE = {articleObject: {article: {}, error: null, loading: true}};

export default function articleReducer(state = INITIAL_STATE, action) {
    let error;
    switch (action.type) {
        case GET_ARTICLE_REQUEST:
            return {...state, articleObject: {article: {}, error: null, loading: true}};
        case GET_ARTICLE_SUCCESS:
            return {...state, articleObject: {article: action.payload, error: null, loading: false}};
        case GET_ARTICLE_FAILURE:
            error = action.payload || {message: action.payload.message};
            return {...state, articleObject: {article: {}, error: error, loading: false}};
        default:
            return state;
    }
}