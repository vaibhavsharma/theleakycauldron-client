/**
 * Created by vaibhav on 20-10-2017.
 */
import React from 'react';
import {Helmet} from "react-helmet";

const AboutPage = () => {
    return (
        <div className="container">
            <Helmet>
                <title>About &middot; The Leaky Cauldron Blog</title>
                <meta name="description" content="A brew of awesomeness with a pinch of magic..."/>
            </Helmet>
            <div className="jumbotron text-center">
                <h1>The Leaky Cauldron Blog</h1>
                <p className="text-primary">A brew of awesomeness with a pinch of magic...</p>
            </div>
            <p>The Leaky Cauldron Blog is the personal blog of Vaibhav Sharma.</p>
            <p>This aims to be a place where I can publish anything and everything that interests me. This can range
                from science and technology to entertainment.</p>
            <p>As you may have already guessed, this blog's name is inspired by the famous pub in Harry Potter. Just
                like that pub this blog will be a magical place where all random interesting things meet.</p>
            <p>This is a place where I want to share the challenges that I have faced in my 20 odd years. I will also
                show you how I overcame them so that others, who may be going through the same won't have go through
                them alone.</p>
            <p>Being a travel enthusiast, I really want this blog to be a place where you can get to know about the
                mysteries and stories of the world. It has been well established that I have a varied interest, so I
                will also sporadically post movie, music and event reviews.</p>
            <p>Occasionally, this blog will feature guest writers who will write about what they truly love. I hope to
                bring to light various unique world views to really live up to my blog's name.</p>
            <p>All in all this, blog has no fixed scope because "True Beauty Lies In Chaos".</p>
            <p>I sincerely hope this blog helps and inspires people to overcome their challenges, learn new things and
                make their lives better.</p>
            <p>As they say, life's a journey and I want us to support each other through each passing day.</p>
            <div className="text-center">~~~ <strong>Mischief Managed</strong> ~~~</div>
        </div>
    );
};

export default AboutPage;