/**
 * Created by vaibhav on 20-10-2017.
 */
import React from 'react';
import {Helmet} from "react-helmet";
import ContactContainer from "../../containers/ContactContainer";

const ContactPage = () => {
    return (
        <div>
            <Helmet>
                <title>Contact &middot; The Leaky Cauldron Blog</title>
                <meta name="description" content="A brew of awesomeness with a pinch of magic..."/>
            </Helmet>
            <ContactContainer/>
        </div>
    );
};

export default ContactPage;