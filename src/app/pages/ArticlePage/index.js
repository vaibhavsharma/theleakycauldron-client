/**
 * Created by vaibhav on 20-10-2017.
 */
import React from 'react';
import PropTypes from 'prop-types';
import ArticleContainer from "../../containers/ArticleContainer";

const ArticlePage = ({match}) => {
    return (
        <ArticleContainer match={match}/>
    );
};

ArticlePage.propTypes = {
    match: PropTypes.object.isRequired,
}

export default ArticlePage;