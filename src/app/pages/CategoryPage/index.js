/**
 * Created by vaibhav on 20-10-2017.
 */
import React from 'react';
import CategoryContainer from "../../containers/CategoryContainer";

const CategoryPage = () => {
    return (
        <CategoryContainer/>
    );
};

export default CategoryPage;