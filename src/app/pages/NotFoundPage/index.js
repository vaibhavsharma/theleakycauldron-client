/**
 * Created by vaibhav on 20-10-2017.
 */
import React from 'react';
import {Helmet} from "react-helmet";
import './style.css';

const NotFoundPage = () => {
    return (
        <div className="container">
            <Helmet>
                <title>404 Not Found &middot; The Leaky Cauldron Blog</title>
                <meta name="description" content="A brew of awesomeness with a pinch of magic..."/>
            </Helmet>
            <div className="not-found text-center">
                <h1>404</h1>
                <p className="text-primary">This is not the page you are looking for...</p>
            </div>
        </div>
    );
};

export default NotFoundPage;