/**
 * Created by vaibhav on 20-10-2017.
 */
import React from 'react';
import {Helmet} from "react-helmet";
import ArticleListContainer from "../../containers/ArticleListContainer";

const HomePage = () => {
    return (
        <div>
            <Helmet>
                <title>Home &middot; The Leaky Cauldron Blog</title>
                <meta name="description" content="A brew of awesomeness with a pinch of magic..."/>
            </Helmet>
            <ArticleListContainer/>
        </div>
    );
};

export default HomePage;