import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import {BrowserRouter} from "react-router-dom";
import configureStore from './core/store';
import 'jquery/src/jquery';
import './resources/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.js';
import './resources/css/main.css';
import App from "./app";
import registerServiceWorker from './registerServiceWorker';

const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App onUpdate={() => window.scrollTo(0, 0)}/>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root'),
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MPPQV7M')
);
registerServiceWorker();
