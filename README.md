# THE LEAKY CAULDRON BLOG
React JS Progressive Web App

## Stack
    -Django
    -MySQL
    -React
    -Redux
    
## Installation
    npm install
    or
    yarn install

## Run (Development Server)
    yarn start
Development Server starts in your default browser at [http://localhost:3000](http://localhost:3000).

## Production Build
    yarn build
Production optimized build generated in build folder.